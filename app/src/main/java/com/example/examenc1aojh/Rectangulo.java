package com.example.examenc1aojh;

public class Rectangulo {
    private int base;
    private int altura;

    public Rectangulo(int base, int altura) {
        this.base = base;
        this.altura = altura;
    }

    // Resto de métodos de la Perimetro

    public float area() {
        return base * altura ;
    }

    public Float Perimetro() {
        return Float.valueOf(base + base + altura + altura);
    }

}
