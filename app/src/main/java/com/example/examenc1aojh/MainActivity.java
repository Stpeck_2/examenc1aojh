package com.example.examenc1aojh;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    // Declaración de variables para los elementos de la interfaz de usuario
    private Button btnIngresar;
    private Button btnSalir;
    private EditText txtUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inicialización de los componentes de la interfaz de usuario
        iniciarComponentes();

        // Asignar listeners a los botones
        btnIngresar.setOnClickListener(v -> ingresar());
        btnSalir.setOnClickListener(v -> salir());
    }

    private void iniciarComponentes() {
        // Asignar los botones y el campo de texto a las variables correspondientes
        btnSalir = findViewById(R.id.btnSalir);
        btnIngresar = findViewById(R.id.btnIngresar);
        txtUsuario = findViewById(R.id.txtUsuario);
    }

    private void ingresar() {
        // Obtener el texto ingresado en el campo de texto txtUsuario
        String strUsuario = txtUsuario.getText().toString();

        // Crear un objeto Bundle para pasar el valor del usuario a la siguiente actividad
        Bundle bundle = new Bundle();
        bundle.putString("usuario", strUsuario);

        // Crear un Intent para iniciar la nueva actividad (ReciboNominaActivity)
        Intent intent = new Intent(MainActivity.this, RectanguloActivity.class);

        // Asignar el Bundle al Intent
        intent.putExtras(bundle);

        // Iniciar la nueva actividad
        startActivity(intent);
    }

    private void salir() {
        // Mostrar un cuadro de diálogo de confirmación para salir de la aplicación
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora Nómina");
        confirmar.setMessage("¿Desea salir de la aplicación?");

        // Acción positiva del cuadro de diálogo: cerrar la actividad actual (MainActivity)
        confirmar.setPositiveButton("Confirmar", (dialogInterface, which) -> finish());

        // Acción negativa del cuadro de diálogo: no hacer nada
        confirmar.setNegativeButton("Cancelar", (dialogInterface, which) -> {});

        // Mostrar el cuadro de diálogo
        confirmar.show();
    }
}